//Version: v1.0.0
package controllers

import (
	"git.forms.io/isaving/sv/ssv0000006/models"
	"git.forms.io/isaving/sv/ssv0000006/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv0000006Controller struct {
	controllers.CommTCCController
}

func (*Ssv0000006Controller) ControllerName() string {
	return "Ssv0000006Controller"
}

// @Desc ssv0000006 controller
// @Description Entry
// @Param ssv0000006 body models.SSV0000006I true "body for user content"
// @Success 200 {object} models.SSV0000006O
// @router /ssv0000006 [post]
// @Author
// @Date 2020-12-12
func (c *Ssv0000006Controller) Ssv0000006() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv0000006Controller.Ssv0000006 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()
	
	ssv0000006I := &models.SSV0000006I{}
	if err := models.UnPackRequest(c.Req.Body, ssv0000006I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv0000006I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv0000006 := &services.Ssv0000006Impl{}
	ssv0000006.New(c.CommTCCController)
	ssv0000006.Ssv000006I = ssv0000006I
	ssv0000006Compensable := services.Ssv0000006Compensable

	proxy, err := aspect.NewDTSProxy(ssv0000006, ssv0000006Compensable, c.DTSCtx)
	if err != nil {
		log.Errorf("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv0000006I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv0000006O, ok := rsp.(*models.SSV0000006O); ok {
		if responseBody, err := models.PackResponse(ssv0000006O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv0000006 Controller
// @Description ssv0000006 controller
// @Param Ssv0000006 body models.SSV0000006I true body for SSV0000006 content
// @Success 200 {object} models.SSV0000006O
// @router /create [post]
/*func (c *Ssv0000006Controller) SWSsv0000006() {
	//Here is to generate API documentation, no need to implement methods
}*/
