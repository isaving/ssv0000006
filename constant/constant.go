//Version: v0.01
package constant

//define error code
const (
	ERRCODE1  = "SVCM000001"
	ERRCODE2  = "SVCM000002"
	ERRCODE3  = ""
	ERRCODE4  = "AC00000004"
	ERRCODE5  = "SV77000012"
	ERRCODE6  = "SV77000013"
	ERRCODE7  = "SV77000014"
	ERRCODE8  = "SV77000005"
	ERRCODE9  = "SV77000007" //FezDocNm cannot be empty
	ERRCODE10 = "SV77000008" //Need input FficialsName1 and FficialsId1
	ERRCODE11 = "SV77000010" //Frozen application number cannot be empty
	ERRCODE12 = "SV77000015"
	ERRCODE13 = "SV77000016"
	ERRCODE14 = "SV77000006" //FezMechType is wrong
	ERRCODE15 = "SV77000017" //	Added freeze record error
	ERRCODE16 = "SV77000023" //Bank amount freezing does not allow excessive freezing
	ERRCODE17 = "SV77000024" //The amount is greater than 0 and the length is 18 digits with two decimal places
	ERRCODE18 = "AC00000018"
	ERRCODE19 = "AC00000019"
	ERRCODE20 = "AC00000020"

	DLSUPCREATEDTS = "DlsupCreateDTS"
	DLSUUPDATEDTS  = "DlsuUpdateDTS"
)
const (
	DAACCAN1 = "DAACCAN1"
	SV100026 = "SV100026"
)

//Define common header
const (
	DTS_TOPIC_TYPE = "DTS"
	TRN_TOPIC_TYPE = "TRN"
	KEY_TYPE_RSA   = "RSA"
	KEY_TYPE_AES   = "AES"
	DLS_TYPE_CMM   = "CMM"
	DLS_ID_COMMON  = "common"
	DLS_TYPE_CON   = "CON"
	DLS_TYPE_ACC   = "ACC"
	DLS_TYPE_ACT   = "ACT" // 核算账号切片类型
	DLS_TYPE_CUS   = "CUS"
	DLS_TYPE_ACG   = "ACG"
	DLS_TYPE_PRD   = "PRD"
	DLS_TYPE_PHN   = "PHN"
	DLS_TYPE_IDN   = "IDN"
	DLS_TYPE_ODR   = "ODR" //FOR ORDER
	DLS_TYPE_MER   = "MER" //FOR merchant
	DLS_TYPE_PPS   = "PPS" //For PP service
	DLS_TYPE_HUI   = "HUI" //For HU id
	DLS_TYPE_ORG   = "ORG" //For organization
	DLS_TYPE_HCD   = "HCD" //For Hcode
	DLS_TYPE_WID   = "WID" //For wallet WalletId
	DLS_TYPE_SCM   = "SCM" //存款公共
)
