current_dir=$(shell pwd)
app_version=v2.0
project_name=$(shell basename "${current_dir}" )
work_dir=/data/jenkins_home/workspace

test:
	chmod +x ./scripts/*.sh
	docker run --rm -v $(work_dir)/$(project_name)/$(project_name):/go/src/git.forms.io/isaving/sv/$(project_name) --name build_$(project_name) golang:1.12.5 /bin/bash /go/src/git.forms.io/isaving/sv/$(project_name)/scripts/test.sh

build:
	chmod +x ./scripts/*.sh
	docker run --rm -v $(work_dir)/$(project_name)/$(project_name):/go/src/git.forms.io/isaving/sv/$(project_name) --name build_$(project_name) golang:1.12.5 /bin/bash /go/src/git.forms.io/isaving/sv/$(project_name)/scripts/build.sh

clean:
	rm $(project_name)
	rm -rf bin
	rm -rf deploy


image:
	sed 's/{{PROJECT_NAME}}/$(project_name)/' Dockerfile.template > Dockerfile
	docker build -t  $(registry)ssv0000006:$(app_version).${BUILD_NUMBER} .

push:
	docker push $(registry)ssv0000006:$(app_version).${BUILD_NUMBER}
	docker rmi $(registry)ssv0000006:$(app_version).${BUILD_NUMBER}

compile:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build .
