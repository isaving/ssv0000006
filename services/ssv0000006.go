//Version: v1.0.0
package services

import (
	"git.forms.io/isaving/sv/ssv0000006/constant"
	"git.forms.io/isaving/sv/ssv0000006/models"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)

var Ssv0000006Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv0000006",
	ConfirmMethod: "ConfirmSsv0000006",
	CancelMethod:  "CancelSsv0000006",
}

type Ssv0000006 interface {
	TrySsv0000006(*models.SSV0000006I) (*models.SSV0000006O, error)
	ConfirmSsv0000006(*models.SSV0000006I) (*models.SSV0000006O, error)
	CancelSsv0000006(*models.SSV0000006I) (*models.SSV0000006O, error)
}

type Ssv0000006Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
	Ssv000006O  *models.SSV0000006O
	Ssv000006I  *models.SSV0000006I
	Ssv1000026O *models.SV100026O
}

// @Desc Ssv0000006 process
// @Author
// @Date 2020-12-12
func (impl *Ssv0000006Impl) TrySsv0000006(ssv0000006I *models.SSV0000006I) (ssv0000006O *models.SSV0000006O, err error) {
	//TODO Service Business Process,No Need Validate Paramer,Paramer Has Already Validate in Controller
	impl.Ssv000006I = ssv0000006I

	//call Froze BFS
	if err := impl.FrozeBFS(); err != nil {
		return nil, err
	}

	ssv0000006O = &models.SSV0000006O{
		FezBusNm:  impl.Ssv1000026O.FezBusNm,
		FnfezFwNm: impl.Ssv1000026O.FnfezFwNm,
	}

	return ssv0000006O, nil
}

func (impl *Ssv0000006Impl) ConfirmSsv0000006(ssv0000006I *models.SSV0000006I) (ssv0000006O *models.SSV0000006O, err error) {
	//TODO Business  confirm Process
	log.Debug("Start confirm ssv0000006")
	return nil, nil
}

func (impl *Ssv0000006Impl) CancelSsv0000006(ssv0000006I *models.SSV0000006I) (ssv0000006O *models.SSV0000006O, err error) {
	//TODO Business  cancel Process
	log.Debug("Start cancel ssv0000006")
	return nil, nil
}

//Froze BFS
func (impl *Ssv0000006Impl) FrozeBFS() error {
	SSV1000026I := models.SV100026I{
		FezMdsNm:      impl.Ssv000006I.FezMdsNm,
		FezMdsTyp:     impl.Ssv000006I.FezMdsTyp,
		FezAgrmt:      impl.Ssv000006I.FezAgrmt,
		FezAgrmtTyp:   impl.Ssv000006I.FezAgrmtTyp,
		FezAccuntNme:  impl.Ssv000006I.FezAccuntNme,
		FezMode:       impl.Ssv000006I.FezMode,
		FezCur:        impl.Ssv000006I.FezCur,
		CashTranFlag:  impl.Ssv000006I.CashTranFlag,
		FezMechType:   impl.Ssv000006I.FezMechType,
		FezMechName:   impl.Ssv000006I.FezMechName,
		FezDocNm:      impl.Ssv000006I.FezDocNm,
		FezApNm:       impl.Ssv000006I.FezApNm,
		FezAmt:        impl.Ssv000006I.FezAmt,
		FezReason:     impl.Ssv000006I.FezReason,
		EffctivDate:   impl.Ssv000006I.EffctivDate,
		FezDueDate:    impl.Ssv000006I.FezDueDate,
		FficialsName1: impl.Ssv000006I.FficialsName1,
		FficialsId1:   impl.Ssv000006I.FficialsId1,
		FficialsName2: impl.Ssv000006I.FficialsName2,
		FficialsId2:   impl.Ssv000006I.FficialsId2,
		FezMech:       impl.Ssv000006I.FezMech,
		FezEm:         impl.Ssv000006I.FezEm,
		FezAuthEm:     impl.Ssv000006I.FezAuthEm,
		Remarks:       impl.Ssv000006I.Remarks,
		CstmrId:       impl.Ssv000006I.CstmrId,
		CstmrTyp:      impl.Ssv000006I.CstmrTyp,
		BnkFezTpy:     impl.Ssv000006I.BnkFezTpy,
	}
	reqBody, err := models.PackRequest(SSV1000026I)
	if nil != err {
		return errors.New("Packing failed", constant.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constant.SV100026, reqBody)
	if err != nil {
		return err
	}

	SSV1000026O := &models.SV100026O{}
	err = SSV1000026O.UnPackResponse(resBody)
	if err != nil {
		return errors.New("UnPacking failed！", constant.ERRCODE2)
	}
	impl.Ssv1000026O = SSV1000026O
	return nil
}
