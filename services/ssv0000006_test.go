//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/sv/ssv0000006/models"
	"testing"
)

var SV100026 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
       "FezBusNm":"123"
    }
}`

func (this *Ssv0000006Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "SV100026":
		responseData = []byte(SV100026)

	}

	return responseData, nil
}

func TestSsv60000006Impl_Ssv0000000(t *testing.T) {

	Ssv00000061Impl := new(Ssv0000006Impl)

	_, _ = Ssv00000061Impl.TrySsv0000006(&models.SSV0000006I{
		FezMdsNm:      "1",
		FezMdsTyp:     "1",
		FezAgrmt:      "1",
		FezAgrmtTyp:   "1",
		FezAccuntNme:  "1",
		FezMode:       "1",
		FezCur:        "1",
		CashTranFlag:  "1",
		FezMechType:   "1",
		FezMechName:   "1",
		FezDocNm:      "1",
		FezApNm:       "1",
		FezAmt:        0,
		FezReason:     "1",
		EffctivDate:   "1",
		FezDueDate:    "1",
		FficialsName1: "1",
		FficialsId1:   "1",
		FficialsName2: "1",
		FficialsId2:   "1",
		FezMech:       "1",
		FezEm:         "1",
		FezAuthEm:     "1",
		Remarks:       "1",
		CstmrId:       "1",
		CstmrTyp:      "1",
	})

	_, _ = Ssv00000061Impl.ConfirmSsv0000006(&models.SSV0000006I{})

	_, _ = Ssv00000061Impl.CancelSsv0000006(&models.SSV0000006I{})
}
