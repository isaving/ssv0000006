package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRWB0I struct {
	BizFolnNo	string `validate:"required,max=32"`
	SysFolnNo	string `validate:"required,max=32"`
	SeqNo	int
}

type DAACRWB0O struct {
	AcctgAcctNo   string
	Status        string
	Currency      string
	WorkDate      string
	MgmtOrgId     string
	AcctingOrgId  string
	BizFolnNo     string
	SysFolnNo     string
	SeqNo         int
	ChnlType      string
	DealType      string
	CalcBgnDate   string
	CalcEndData   string
	CalcDays      int64
	CalcPrimInt   float64
	BalanceType   string
	IntPlanNo     string
	CalcIntRate   float64
	CalcPattern   string
	DealStatus    string
	MsgCode       string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	PeriodNum     int64

}

type DAACRWB0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRWB0I
}

type DAACRWB0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRWB0O
}

type DAACRWB0RequestForm struct {
	Form []DAACRWB0IDataForm
}

type DAACRWB0ResponseForm struct {
	Form []DAACRWB0ODataForm
}

// @Desc Build request message
func (o *DAACRWB0RequestForm) PackRequest(DAACRWB0I DAACRWB0I) (responseBody []byte, err error) {

	requestForm := DAACRWB0RequestForm{
		Form: []DAACRWB0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRWB0I",
				},
				FormData: DAACRWB0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRWB0RequestForm) UnPackRequest(request []byte) (DAACRWB0I, error) {
	DAACRWB0I := DAACRWB0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRWB0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRWB0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRWB0ResponseForm) PackResponse(DAACRWB0O DAACRWB0O) (responseBody []byte, err error) {
	responseForm := DAACRWB0ResponseForm{
		Form: []DAACRWB0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRWB0O",
				},
				FormData: DAACRWB0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRWB0ResponseForm) UnPackResponse(request []byte) (DAACRWB0O, error) {

	DAACRWB0O := DAACRWB0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRWB0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRWB0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRWB0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
