package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRTC3I struct {
	RecordsNum int
	Records []DAACRTC3IRecords
}

type DAACRTC3IRecords struct {
	AcctgAcctNo		string `validate:"required,max=40"`
	PeriodNum		int64
}

type DAACRTC3O struct {
	LogTotCount string    `json:"LogTotCount"`
	Records     []DAACRTC3ORecords `json:"Records"`
}

type DAACRTC3ORecords struct {
	AccmCmpdAmt         int    `json:"AccmCmpdAmt"`
	AccmIntSetlAmt      int    `json:"AccmIntSetlAmt"`
	AccmWdAmt           int    `json:"AccmWdAmt"`
	AcctgAcctNo         string `json:"AcctgAcctNo"`
	AcruUnstlIntr       int    `json:"AcruUnstlIntr"`
	AlrdyTranOffshetInt int    `json:"AlrdyTranOffshetInt"`
	BalanceType         string `json:"BalanceType"`
	CavInt              int    `json:"CavInt"`
	CurrIntEndDate      string `json:"CurrIntEndDate"`
	CurrIntStDate       string `json:"CurrIntStDate"`
	DcValueInt          int    `json:"DcValueInt"`
	FrzAmt              int    `json:"FrzAmt"`
	LastCalcDate        string `json:"LastCalcDate"`
	LastMaintBrno       string `json:"LastMaintBrno"`
	LastMaintDate       string `json:"LastMaintDate"`
	LastMaintTell       string `json:"LastMaintTell"`
	LastMaintTime       string `json:"LastMaintTime"`
	OnshetInt           int    `json:"OnshetInt"`
	PeriodNum           int    `json:"PeriodNum"`
	RecordNo            int    `json:"RecordNo"`
	RepaidInt           int    `json:"RepaidInt"`
	RepaidIntAmt        int    `json:"RepaidIntAmt"`
	Status              string `json:"Status"`
	UnpaidInt           int    `json:"UnpaidInt"`
	UnpaidIntAmt        int    `json:"UnpaidIntAmt"`
}
type DAACRTC3IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRTC3I
}


type DAACRTC3ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRTC3O
}

type DAACRTC3RequestForm struct {
	Form []DAACRTC3IDataForm
}

type DAACRTC3ResponseForm struct {
	Form []DAACRTC3ODataForm
}

// @Desc Build request message
func (o *DAACRTC3RequestForm) PackRequest(DAACRTC3I DAACRTC3I) (responseBody []byte, err error) {

	requestForm := DAACRTC3RequestForm{
		Form: []DAACRTC3IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTC3I",
				},
				FormData: DAACRTC3I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRTC3RequestForm) UnPackRequest(request []byte) (DAACRTC3I, error) {
	DAACRTC3I := DAACRTC3I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTC3I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTC3I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRTC3ResponseForm) PackResponse(DAACRTC3O DAACRTC3O) (responseBody []byte, err error) {
	responseForm := DAACRTC3ResponseForm{
		Form: []DAACRTC3ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTC3O",
				},
				FormData: DAACRTC3O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRTC3ResponseForm) UnPackResponse(request []byte) (DAACRTC3O, error) {

	DAACRTC3O := DAACRTC3O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTC3O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTC3O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRTC3I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
