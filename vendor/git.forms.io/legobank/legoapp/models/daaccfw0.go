package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCFW0I struct {
	HostTranSerialNo          string `validate:"required,max=42"`
	BussDate                  string
	BussTime                  string
	HostTranSeq               int64
	PeripheralSysWorkday      string
	PeripheralSysWorktime     string
	PeripheralTranSerialNo    string
	PeripheralTranSeq         float64
	BookedWorkday             string
	AcctingTime               string
	LiquidationDate           string
	LiquidationTime           string
	TranOrgId                 string
	AgentOrgId                string
	TranTeller                string
	AuthTeller                string
	ReviewTeller              string
	TranChannel               string
	AccessChannel             string
	TerminalNo                string
	BussSys                   string
	TrasactionCode            string
	FunctionCode              string
	ApprovalNo                string
	ClearingBussType          string
	ProdCode                  string
	ProdSeq                   float64
	BussType                  string
	BussCategories            string
	DebitFlag                 string
	LocalBankFlag             string
	Country                   string
	OtherBankFlag             string
	AcctingOrgId              string
	LocalAcctDiff             string
	LocalTranDetailType       string
	CustId                    string
	TheMerchantNo             string
	BussAcctNo                string
	AcctNo                    string
	AcctgAcctNo               string
	SubjectNo                 string
	SubjectBreakdown          string
	MediaType                 string
	LocalMediaPrefix          string
	MediaNo                   string
	CustDiff                  string
	AmtType                   string
	DurationOfDep             float64
	DaysOfDep                 float64
	AcctingCode1              string
	AcctingCode2              string
	AcctingCode3              string
	AcctingCode4              string
	EventIndication           string
	EventBreakdown            string
	CustEvent1                string
	CustEvent2                string
	Currency                  string
	BanknoteFlag              string
	Amt                       float64
	AcctBal                   float64
	CashTranFlag              string
	ValueDate                 string
	ExchRestType              string
	AntiTradingMark           string
	CorrectTradingFlag        string
	OriginalTradingDay        string
	OriginalHostTranSerialNo  float64
	OriginalTradeTellerNo     string
	AcctManagerNo             string
	MessageCode               string
	AbstractCode              string
	UseCode                   string
	SetNo                     string
	WhetherToAllowPunching    string
	ChannelCorrectControlFlag string
	IsSendClearingFlag        string
	IsMakeUpForPayables       string
	LastMaintDate             string
	LastMaintTime             string
	LastMaintBrno             string
	LastMaintTell             string
	BalanceType               string
	RepayPrincipal            float64
	RepayInterest             float64
	LastTranDate              string
}

type DAACCFW0O struct {
	HostTranSerialNo          string `validate:"required,max=42"`
	BussDate                  string
	BussTime                  string
	HostTranSeq               int64
	PeripheralSysWorkday      string
	PeripheralSysWorktime     string
	PeripheralTranSerialNo    string
	PeripheralTranSeq         float64
	BookedWorkday             string
	AcctingTime               string
	LiquidationDate           string
	LiquidationTime           string
	TranOrgId                 string
	AgentOrgId                string
	TranTeller                string
	AuthTeller                string
	ReviewTeller              string
	TranChannel               string
	AccessChannel             string
	TerminalNo                string
	BussSys                   string
	TrasactionCode            string
	FunctionCode              string
	ApprovalNo                string
	ClearingBussType          string
	ProdCode                  string
	ProdSeq                   float64
	BussType                  string
	BussCategories            string
	DebitFlag                 string
	LocalBankFlag             string
	Country                   string
	OtherBankFlag             string
	AcctingOrgId              string
	LocalAcctDiff             string
	LocalTranDetailType       string
	CustId                    string
	TheMerchantNo             string
	BussAcctNo                string
	AcctNo                    string
	AcctgAcctNo               string
	SubjectNo                 string
	SubjectBreakdown          string
	MediaType                 string
	LocalMediaPrefix          string
	MediaNo                   string
	CustDiff                  string
	AmtType                   string
	DurationOfDep             float64
	DaysOfDep                 float64
	AcctingCode1              string
	AcctingCode2              string
	AcctingCode3              string
	AcctingCode4              string
	EventIndication           string
	EventBreakdown            string
	CustEvent1                string
	CustEvent2                string
	Currency                  string
	BanknoteFlag              string
	Amt                       float64
	AcctBal                   float64
	CashTranFlag              string
	ValueDate                 string
	ExchRestType              string
	AntiTradingMark           string
	CorrectTradingFlag        string
	OriginalTradingDay        string
	OriginalHostTranSerialNo  float64
	OriginalTradeTellerNo     string
	AcctManagerNo             string
	MessageCode               string
	AbstractCode              string
	UseCode                   string
	SetNo                     string
	WhetherToAllowPunching    string
	ChannelCorrectControlFlag string
	IsSendClearingFlag        string
	IsMakeUpForPayables       string
	LastMaintDate             string
	LastMaintTime             string
	LastMaintBrno             string
	LastMaintTell             string
	BalanceType               string
	RepayPrincipal            float64
	RepayInterest             float64
	LastTranDate              string
}

type DAACCFW0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCFW0I
}

type DAACCFW0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCFW0O
}

type DAACCFW0RequestForm struct {
	Form []DAACCFW0IDataForm
}

type DAACCFW0ResponseForm struct {
	Form []DAACCFW0ODataForm
}

// @Desc Build request message
func (o *DAACCFW0RequestForm) PackRequest(DAACCFW0I DAACCFW0I) (responseBody []byte, err error) {

	requestForm := DAACCFW0RequestForm{
		Form: []DAACCFW0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCFW0I",
				},
				FormData: DAACCFW0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCFW0RequestForm) UnPackRequest(request []byte) (DAACCFW0I, error) {
	DAACCFW0I := DAACCFW0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCFW0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCFW0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCFW0ResponseForm) PackResponse(DAACCFW0O DAACCFW0O) (responseBody []byte, err error) {
	responseForm := DAACCFW0ResponseForm{
		Form: []DAACCFW0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCFW0O",
				},
				FormData: DAACCFW0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCFW0ResponseForm) UnPackResponse(request []byte) (DAACCFW0O, error) {

	DAACCFW0O := DAACCFW0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCFW0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCFW0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCFW0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
