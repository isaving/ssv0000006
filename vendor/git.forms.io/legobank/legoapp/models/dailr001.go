package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAILR001I struct {

}

type DAILR001O struct {

}

type DAILR001IDataForm struct {
	FormHead CommonFormHead
	FormData DAILR001I
}

type DAILR001ODataForm struct {
	FormHead CommonFormHead
	FormData DAILR001O
}

type DAILR001RequestForm struct {
	Form []DAILR001IDataForm
}

type DAILR001ResponseForm struct {
	Form []DAILR001ODataForm
}

// @Desc Build request message
func (o *DAILR001RequestForm) PackRequest(DAILR001I DAILR001I) (responseBody []byte, err error) {

	requestForm := DAILR001RequestForm{
		Form: []DAILR001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAILR001I",
				},
				FormData: DAILR001I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAILR001RequestForm) UnPackRequest(request []byte) (DAILR001I, error) {
	DAILR001I := DAILR001I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAILR001I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAILR001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAILR001ResponseForm) PackResponse(DAILR001O DAILR001O) (responseBody []byte, err error) {
	responseForm := DAILR001ResponseForm{
		Form: []DAILR001ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAILR001O",
				},
				FormData: DAILR001O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAILR001ResponseForm) UnPackResponse(request []byte) (DAILR001O, error) {

	DAILR001O := DAILR001O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAILR001O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAILR001O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAILR001I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
