package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type I struct {

}

type O struct {

}

type IDataForm struct {
	FormHead CommonFormHead
	FormData I
}

type ODataForm struct {
	FormHead CommonFormHead
	FormData O
}

type RequestForm struct {
	Form []IDataForm
}

type ResponseForm struct {
	Form []ODataForm
}

// @Desc Build request message
func (o *RequestForm) PackRequest(I I) (responseBody []byte, err error) {

	requestForm := RequestForm{
		Form: []IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "I",
				},
				FormData: I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *RequestForm) UnPackRequest(request []byte) (I, error) {
	I := I{}
	if err := json.Unmarshal(request, o); nil != err {
		return I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *ResponseForm) PackResponse(O O) (responseBody []byte, err error) {
	responseForm := ResponseForm{
		Form: []ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "O",
				},
				FormData: O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *ResponseForm) UnPackResponse(request []byte) (O, error) {

	O := O{}

	if err := json.Unmarshal(request, o); nil != err {
		return O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
