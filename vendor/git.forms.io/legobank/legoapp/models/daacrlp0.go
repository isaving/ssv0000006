package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRLP0I struct {
	AcctgAcctNo		string ` validate:"required,max=20"`
	PeriodNum		int64
}

type DAACRLP0O struct {
	AcctgAcctNo              string `json:"AcctgAcctNo"`
	AcctgDate                string `json:"AcctgDate"`
	ActualRepayBal           int    `json:"ActualRepayBal"`
	ActualRepayDate          string `json:"ActualRepayDate"`
	BurningSum               int    `json:"BurningSum"`
	CurrPeriodIntacrBgnDt    string `json:"CurrPeriodIntacrBgnDt"`
	CurrPeriodIntacrEndDt    string `json:"CurrPeriodIntacrEndDt"`
	CurrPeriodRepayDt        string `json:"CurrPeriodRepayDt"`
	CurrentPeriodUnStillPrin int    `json:"CurrentPeriodUnStillPrin"`
	CurrentStatus            string `json:"CurrentStatus"`
	LastMaintBrno            string `json:"LastMaintBrno"`
	LastMaintDate            string `json:"LastMaintDate"`
	LastMaintTell            string `json:"LastMaintTell"`
	LastMaintTime            string `json:"LastMaintTime"`
	PeriodNum                int    `json:"PeriodNum"`
	PlanRepayBal             int    `json:"PlanRepayBal"`
	RepayStatus              string `json:"RepayStatus"`
	TccState                 int    `json:"TccState"`
	ValueDate                string `json:"ValueDate"`
}

type DAACRLP0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRLP0I
}

type DAACRLP0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRLP0O
}

type DAACRLP0RequestForm struct {
	Form []DAACRLP0IDataForm
}

type DAACRLP0ResponseForm struct {
	Form []DAACRLP0ODataForm
}

// @Desc Build request message
func (o *DAACRLP0RequestForm) PackRequest(DAACRLP0I DAACRLP0I) (responseBody []byte, err error) {

	requestForm := DAACRLP0RequestForm{
		Form: []DAACRLP0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLP0I",
				},
				FormData: DAACRLP0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRLP0RequestForm) UnPackRequest(request []byte) (DAACRLP0I, error) {
	DAACRLP0I := DAACRLP0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLP0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLP0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRLP0ResponseForm) PackResponse(DAACRLP0O DAACRLP0O) (responseBody []byte, err error) {
	responseForm := DAACRLP0ResponseForm{
		Form: []DAACRLP0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLP0O",
				},
				FormData: DAACRLP0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRLP0ResponseForm) UnPackResponse(request []byte) (DAACRLP0O, error) {

	DAACRLP0O := DAACRLP0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLP0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLP0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRLP0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
