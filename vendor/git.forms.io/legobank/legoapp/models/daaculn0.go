package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACULN0I struct {
	//输入是个map
}

type DAACULN0O struct {

}

type DAACULN0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACULN0I
}

type DAACULN0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACULN0O
}

type DAACULN0RequestForm struct {
	Form []DAACULN0IDataForm
}

type DAACULN0ResponseForm struct {
	Form []DAACULN0ODataForm
}

// @Desc Build request message
func (o *DAACULN0RequestForm) PackRequest(DAACULN0I DAACULN0I) (responseBody []byte, err error) {

	requestForm := DAACULN0RequestForm{
		Form: []DAACULN0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULN0I",
				},
				FormData: DAACULN0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACULN0RequestForm) UnPackRequest(request []byte) (DAACULN0I, error) {
	DAACULN0I := DAACULN0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACULN0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULN0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACULN0ResponseForm) PackResponse(DAACULN0O DAACULN0O) (responseBody []byte, err error) {
	responseForm := DAACULN0ResponseForm{
		Form: []DAACULN0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULN0O",
				},
				FormData: DAACULN0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACULN0ResponseForm) UnPackResponse(request []byte) (DAACULN0O, error) {

	DAACULN0O := DAACULN0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACULN0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULN0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACULN0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
