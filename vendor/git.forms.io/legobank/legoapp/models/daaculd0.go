package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACULD0I struct {
	TxAcctnDt                 string
	TxTm                      string
	HostTranSerialNo          string ` validate:"required,max=34"`
	HostTranSeq               string ` validate:"required,max=34"`
	HostTranInq               string ` validate:"required,max=6"`
	BookedWorkday             string
	AcctingTime               string
	LiquidationDate           string
	LiquidationTime           string
	TranOrgId                 string
	AgentOrgId                string
	TranTeller                string
	AuthTeller                string
	ReviewTeller              string
	TranChannel               string
	AccessChannel             string
	TerminalNo                string
	BussSys                   string
	TrasactionCode            string
	ClearingBussType          string
	BussType                  string
	BussCategories            string
	DebitFlag                 string
	LocalBankFlag             string
	Country                   string
	OtherBankFlag             string
	AcctingOrgId              string
	LocalAcctDiff             string
	LocalTranDetailType       string
	CustId                    string
	TheMerchantNo             string
	BussAcctNo                string
	AcctNo                    string
	AcctgAcctNo               string
	AmtType                   string
	Currency                  string
	BanknoteFlag              string
	Amt                       float64
	AcctBal                   float64
	CashTranFlag              string
	ValueDate                 string
	ExchRestType              string
	AntiTradingMark           string
	CorrectTradingFlag        string
	OriginalTradingDay        string
	OriginalHostTranSerialNo  string
	OriginalTradeTellerNo     string
	AcctManagerNo             string
	MessageCode               string
	AbstractCode              string
	UseCode                   string
	SetNo                     string
	WhetherToAllowPunching    string
	ChannelCorrectControlFlag string
	IsSendClearingFlag        string
	IsMakeUpForPayables       string
	LastMaintDate             string
	LastMaintTime             string
	LastMaintBrno             string
	LastMaintTell             string
	TccState                  int8
	Abstract                  string
	Status                    int64
}

type DAACULD0O struct {

}

type DAACULD0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACULD0I
}

type DAACULD0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACULD0O
}

type DAACULD0RequestForm struct {
	Form []DAACULD0IDataForm
}

type DAACULD0ResponseForm struct {
	Form []DAACULD0ODataForm
}

// @Desc Build request message
func (o *DAACULD0RequestForm) PackRequest(DAACULD0I DAACULD0I) (responseBody []byte, err error) {

	requestForm := DAACULD0RequestForm{
		Form: []DAACULD0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULD0I",
				},
				FormData: DAACULD0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACULD0RequestForm) UnPackRequest(request []byte) (DAACULD0I, error) {
	DAACULD0I := DAACULD0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACULD0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULD0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACULD0ResponseForm) PackResponse(DAACULD0O DAACULD0O) (responseBody []byte, err error) {
	responseForm := DAACULD0ResponseForm{
		Form: []DAACULD0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULD0O",
				},
				FormData: DAACULD0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACULD0ResponseForm) UnPackResponse(request []byte) (DAACULD0O, error) {

	DAACULD0O := DAACULD0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACULD0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULD0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACULD0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
