package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCTB1I struct {
	AcctgAcctNo   string `validate:"required,max=20"`
	PrinStatus    string
	IntPlanNo     string
	OrgCreate     string
	ValidFlag     string
	Bk1           string
	Bk2           string
	Bk3           string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
}

type DAACCTB1O struct {
	AcctgAcctNo   string
	PrinStatus    string
	IntPlanNo     string
	OrgCreate     string
	ValidFlag     string
	Bk1           string
	Bk2           string
	Bk3           string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
}

type DAACCTB1IDataForm struct {
	FormHead CommonFormHead
	FormData []DAACCTB1I
}

type DAACCTB1ODataForm struct {
	FormHead CommonFormHead
	FormData []DAACCTB1O
}

type DAACCTB1RequestForm struct {
	Form []DAACCTB1IDataForm
}

type DAACCTB1ResponseForm struct {
	Form []DAACCTB1ODataForm
}

// @Desc Build request message
func (o *DAACCTB1RequestForm) PackRequest(DAACCTB1I []DAACCTB1I) (responseBody []byte, err error) {

	requestForm := DAACCTB1RequestForm{
		Form: []DAACCTB1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCTB1I",
				},
				FormData: DAACCTB1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCTB1RequestForm) UnPackRequest(request []byte) ([]DAACCTB1I, error) {
	DAACCTB1I := []DAACCTB1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCTB1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCTB1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCTB1ResponseForm) PackResponse(DAACCTB1O []DAACCTB1O) (responseBody []byte, err error) {
	responseForm := DAACCTB1ResponseForm{
		Form: []DAACCTB1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCTB1O",
				},
				FormData: DAACCTB1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCTB1ResponseForm) UnPackResponse(request []byte) ([]DAACCTB1O, error) {

	DAACCTB1O := []DAACCTB1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCTB1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCTB1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCTB1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
