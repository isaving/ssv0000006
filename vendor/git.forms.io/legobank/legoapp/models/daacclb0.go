package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCLB0I struct {
	AcctgAcctNo      string ` validate:"required,max=20"`
	BalanceType      float64
	Balance          float64
	BalanceYesterday float64
	LastMaintDate    string
	LastMaintTime    string
	LastMaintBrno    string
	LastMaintTell    string
}

type DAACCLB0O struct {
	AcctgAcctNo      string
	BalanceType      float64
	Balance          float64
	BalanceYesterday float64
	LastMaintDate    string
	LastMaintTime    string
	LastMaintBrno    string
	LastMaintTell    string
}

type DAACCLB0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCLB0I
}

type DAACCLB0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCLB0O
}

type DAACCLB0RequestForm struct {
	Form []DAACCLB0IDataForm
}

type DAACCLB0ResponseForm struct {
	Form []DAACCLB0ODataForm
}

// @Desc Build request message
func (o *DAACCLB0RequestForm) PackRequest(DAACCLB0I DAACCLB0I) (responseBody []byte, err error) {

	requestForm := DAACCLB0RequestForm{
		Form: []DAACCLB0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLB0I",
				},
				FormData: DAACCLB0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCLB0RequestForm) UnPackRequest(request []byte) (DAACCLB0I, error) {
	DAACCLB0I := DAACCLB0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLB0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLB0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCLB0ResponseForm) PackResponse(DAACCLB0O DAACCLB0O) (responseBody []byte, err error) {
	responseForm := DAACCLB0ResponseForm{
		Form: []DAACCLB0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLB0O",
				},
				FormData: DAACCLB0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCLB0ResponseForm) UnPackResponse(request []byte) (DAACCLB0O, error) {

	DAACCLB0O := DAACCLB0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLB0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLB0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCLB0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
