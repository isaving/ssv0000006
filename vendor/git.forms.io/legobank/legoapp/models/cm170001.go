package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type CM170001RequestForm struct {
	Form []CM170001IDataForm
}

type CM170001IDataForm struct {
	FormHead CommonFormHead
	FormData CM170001I
}
type CM170001I struct {
	/*CrdtAplySn string `Required;valid:"MaxSize(34)"` //授信申请流水号  // Credit application serial number
	CustNo               string `valid:"MaxSize(14)"` // 客户编号*/
	UserId string
	Api string
}

// @Desc Parsing request message
func (w *CM170001RequestForm) UnPackRequest(req []byte) (CM170001I, error) {
	CM170001I := CM170001I{}
	if err := json.Unmarshal(req, w); nil != err {
		return CM170001I, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	if len(w.Form) < 1 {
		return CM170001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}
func (w *CM170001RequestForm) PackRequest(CM170001IData CM170001I) (res []byte, err error) {
	resForm := CM170001RequestForm{
		Form: []CM170001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "CM170001I001",
				},
				FormData: CM170001IData,
			},
		},
	}

	rspBody, err := json.Marshal(resForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, "")
	}

	return rspBody, nil
}

type CM170001ResponseForm struct {
	Form []CM170001ODataForm
}

type CM170001ODataForm struct {
	FormHead CommonFormHead
	FormData CM170001O
}

type CM170001O struct {
	/*TotalPageNum int
	PageNo       int
	CustomerInfo []Records*/
	/*AvailQuotaAmt  int         `json:"AvailQuotaAmt"`
	ChgAplyNo      interface{} `json:"ChgAplyNo"`
	CurCd          string      `json:"CurCd"`
	CurrCrdtQta    int         `json:"CurrCrdtQta"`
	CustNm         string      `json:"CustNm"`
	CustNo         string      `json:"CustNo"`
	ProdId         string      `json:"ProdId"`
	ProdName       string      `json:"ProdName"`
	QtaAprvlDt     string      `json:"QtaAprvlDt"`
	QtaMatrDt      string      `json:"QtaMatrDt"`
	QtaStusCd      string      `json:"QtaStusCd"`
	RevlQtaFlg     string      `json:"RevlQtaFlg"`
	WhtlPreCrdtQta int         `json:"WhtlPreCrdtQta"`*/
	Status string   `json:"Status"`
}



// @Desc Build response message
func (w *CM170001ResponseForm) PackResponse(Data CM170001O) (res []byte, err error) {
	resForm := CM170001ResponseForm{
		Form: []CM170001ODataForm{
			CM170001ODataForm{
				FormHead: CommonFormHead{
					FormId: "CM170001O",
				},
				FormData: Data,
			},
		},
	}

	rspBody, err := json.Marshal(resForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

func (w *CM170001ResponseForm) UnPackResponse(req []byte) (CM170001O, error) {
	CM170001O := CM170001O{}
	if err := json.Unmarshal(req, w); nil != err {
		return CM170001O, errors.Wrap(err, 0, "")
	}

	if len(w.Form) < 1 {
		return CM170001O, errors.Errorf("", "CM170001ResponseForm.UnPackResponse")
	}

	return w.Form[0].FormData, nil
}

func (w *CM170001I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
