package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAUREDISI struct {
	PrefixKey string    `json:"PrefixKey";valid:Required` //键的前缀(例如：表名.主键.)
	Key       string    `json:"Key";valid:Required`       //键的值(例如：主键的值)
	HashSet   DAIL8R01I //系统状态控制信息表 (key：value=字段名：值)
}

type DAUREDISO struct {
	Status string //执行状态 OK 新建或者更新成功
}

type DAUREDISIDataForm struct {
	FormHead CommonFormHead
	FormData DAUREDISI
}

type DAUREDISODataForm struct {
	FormHead CommonFormHead
	FormData DAUREDISO
}

type DAUREDISRequestForm struct {
	Form []DAUREDISIDataForm
}

type DAUREDISResponseForm struct {
	Form []DAUREDISODataForm
}

// @Desc Build request message
func (o *DAUREDISRequestForm) PackRequest(DAUREDISI DAUREDISI) (responseBody []byte, err error) {

	requestForm := DAUREDISRequestForm{
		Form: []DAUREDISIDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAUREDISI",
				},
				FormData: DAUREDISI,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAUREDISRequestForm) UnPackRequest(request []byte) (DAUREDISI, error) {
	DAUREDISI := DAUREDISI{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAUREDISI, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAUREDISI, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAUREDISResponseForm) PackResponse(DAUREDISO DAUREDISO) (responseBody []byte, err error) {
	responseForm := DAUREDISResponseForm{
		Form: []DAUREDISODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAUREDISO",
				},
				FormData: DAUREDISO,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAUREDISResponseForm) UnPackResponse(request []byte) (DAUREDISO, error) {

	DAUREDISO := DAUREDISO{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAUREDISO, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAUREDISO, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAUREDISI) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
