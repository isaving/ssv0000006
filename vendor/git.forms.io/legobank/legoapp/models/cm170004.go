package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type CM170004I struct {
	Flag string //标志位，1、2、3、4.... 传给下游das 做特殊处理
}

type CM170004O struct {
	PageTotCount int
	PageNo       int
	Records      []CM170004Orecords
}

type CM170004Orecords struct {
	KeprcdNo        string
	SysStusCd       string
	SysCtofModeCd   string
	BatBizDt        string
	NxtoneBizDt     string
	LstoneBizDt     string
	CtofTm          string
	TranOnlineTm    string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	TccState        string
	OnlineBizDt     string
}

type CM170004IDataForm struct {
	FormHead CommonFormHead
	FormData CM170004I
}

type CM170004ODataForm struct {
	FormHead CommonFormHead
	FormData CM170004O
}

type CM170004RequestForm struct {
	Form []CM170004IDataForm
}

type CM170004ResponseForm struct {
	Form []CM170004ODataForm
}

// @Desc Build request message
func (o *CM170004RequestForm) PackRequest(CM170004I CM170004I) (responseBody []byte, err error) {

	requestForm := CM170004RequestForm{
		Form: []CM170004IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "CM170004I",
				},
				FormData: CM170004I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *CM170004RequestForm) UnPackRequest(request []byte) (CM170004I, error) {
	CM170004I := CM170004I{}
	if err := json.Unmarshal(request, o); nil != err {
		return CM170004I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return CM170004I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *CM170004ResponseForm) PackResponse(CM170004O CM170004O) (responseBody []byte, err error) {
	responseForm := CM170004ResponseForm{
		Form: []CM170004ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "CM170004O",
				},
				FormData: CM170004O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *CM170004ResponseForm) UnPackResponse(request []byte) (CM170004O, error) {

	CM170004O := CM170004O{}

	if err := json.Unmarshal(request, o); nil != err {
		return CM170004O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return CM170004O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *CM170004I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
