package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC020006I struct {
	GlobalBizSeqNo	  string
	SrcBizSeqNo		  string
	LoanAcctiAcctNo   string  `valid:"Required;MaxSize(20)"`
	TradeDate         string  `valid:"Required;MaxSize(20)"` //交易日期
	TxnAmt            float64 `valid:"Required"`             //交易金额
	CurCd             string  `valid:"Required;MaxSize(3)"`
	RcrdacctAcctnDt   string  `valid:"MaxSize(10)"`
	RcrdacctAcctnTm   string  `valid:"MaxSize(20)"`
	LiqdDt            string  `valid:"MaxSize(8)"`
	LiqdTm            string  `valid:"MaxSize(8)"`
	TxOrgNo           string  `valid:"MaxSize(4)"`
	AgenOrgNo         string  `valid:"MaxSize(4)"`
	TxTelrNo          string  `valid:"MaxSize(6)"`
	AuthTelrNo        string  `valid:"MaxSize(6)"`
	RchkTelrNo        string  `valid:"MaxSize(6)"`
	LunchChnlTypCd    string  `valid:"MaxSize(4)"`
	AccessChnlTypCd   string  `valid:"MaxSize(4)"`
	TerminalNo        string  `valid:"MaxSize(20)"`
	BizSysNo          string  `valid:"MaxSize(3)"`
	TranCd            string  `valid:"MaxSize(10)"`
	LiqdBizTypCd      string  `valid:"MaxSize(2)"`
	BizKindCd         string  `valid:"MaxSize(3)"`
	BizClsfCd         string  `valid:"MaxSize(20)"`
	DebitCrdtFlg      string  `valid:"MaxSize(1)"`
	MbankFlg          string  `valid:"MaxSize(1)"`
	StateAndRgnCd     string  `valid:"MaxSize(4)"`
	OthbnkBnkNo       string  `valid:"MaxSize(14)"`
	OpenAcctOrgNo     string  `valid:"MaxSize(4)"`
	MbankAcctFlg      string  `valid:"MaxSize(1)"`
	TxDtlTypCd        string  `valid:"MaxSize(1)"`
	CustNo            string  `valid:"MaxSize(14)"`
	MerchtNo          string  `valid:"MaxSize(32)"`
	SvngAcctNo        string  `valid:"MaxSize(32)"`
	CardNoOrAcctNo    string  `valid:"MaxSize(32)"`
	FinTxAmtTypCd     string  `valid:"MaxSize(1)"`
	CashrmtFlgCd      string  `valid:"MaxSize(1)"`
	AcctBal           float64
	CashTxRcptpymtCd  string `valid:"MaxSize(1)"`
	FrnexcgStlManrCd  string `valid:"MaxSize(8)"`
	PostvReblnTxFlgCd string `valid:"MaxSize(1)"`
	RvrsTxFlgCd       string `valid:"MaxSize(1)"`
	OrginlTxAcctnDt   string `valid:"MaxSize(8)"`
	OrginlHostTxSn    string `valid:"MaxSize(10)"`
	OrginlTxTelrNo    string `valid:"MaxSize(6)"`
	CustMgrTelrNo     string `valid:"MaxSize(6)"`
	LoanUsageCd       string `valid:"MaxSize(4)"`
	AcctBookNo        string `valid:"MaxSize(15)"`
	PmitRvrsFlg       string `valid:"MaxSize(1)"`
	ChnlRvrsCtrlFlgCd string `valid:"MaxSize(4)"`
	GvayLiqdFlg       string `valid:"MaxSize(1)"`
	MndArapFlg        string `valid:"MaxSize(1)"`
	ArrRecCnt         int
	Records           []interface{} `valid:"Required"`
	ValueDate			string
	MessageCode			string
	AbstractCode		string
	Abstract			string
}

type AC020006O struct {
	AcctgAcctNo		string
	Currency		string
	Status			string
	Balance			string
	ArrRecCnt		string
	Records			string
}

// @Desc Build request message
func (o *AC020006I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *AC020006I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *AC020006O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC020006O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *AC020006I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

