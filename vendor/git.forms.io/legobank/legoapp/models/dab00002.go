package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAB00002I struct {
	Id         int
	ProjId     int
	FlowId     string
	ExecId     int
	JobId      string
	ReplyTopic string
	RetCode    string
	RetMsg     string
	PubMap     map[string]string

	Limit  int
	EnvMap map[string]string
}

type DAB00002O struct {
	//Start string //处理状态 ok
}

type BatchRequestEnvArgs struct {
	Key   string
	Value string
}

type BatchResponseEnvArgs struct {
	Key   string
	Value string
}

func (o *DAB00002I) UnPackRequest(requestBody []byte) (err error) {
	//mapOne := make(map[string]interface{})
	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *DAB00002O) PackResponse() (responseBody []byte, err error) {

	/*commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}*/

	responseBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

func (o *DAB00002I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}
