package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC020010I struct {
	AcctgAcctNo string `validate:"max=32,required"` // 贷款核算账
	Records     []interface{}
}

type AC020010O struct {
	Records		[]interface{}
	AcctgAcctNo	string
}

type AC020010IDataForm struct {
	FormHead CommonFormHead
	FormData AC020010I
}

type AC020010ODataForm struct {
	FormHead CommonFormHead
	FormData AC020010O
}

type AC020010RequestForm struct {
	Form []AC020010IDataForm
}

type AC020010ResponseForm struct {
	Form []AC020010ODataForm
}

// @Desc Build request message
func (o *AC020010RequestForm) PackRequest(AC020010I AC020010I) (responseBody []byte, err error) {

	requestForm := AC020010RequestForm{
		Form: []AC020010IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020010I",
				},
				FormData: AC020010I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC020010RequestForm) UnPackRequest(request []byte) (AC020010I, error) {
	AC020010I := AC020010I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC020010I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC020010I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC020010ResponseForm) PackResponse(AC020010O AC020010O) (responseBody []byte, err error) {
	responseForm := AC020010ResponseForm{
		Form: []AC020010ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020010O",
				},
				FormData: AC020010O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC020010ResponseForm) UnPackResponse(request []byte) (AC020010O, error) {

	AC020010O := AC020010O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC020010O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC020010O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC020010I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
