package models

import "testing"

const (
	IL100080_REQUEST_DATA = ``
	IL100080_RESPONSE_DATA =``
)

func TestIL100080I(t *testing.T) {

	il100080I := &IL100080I{}
	if err := il100080I.UnPackRequest([]byte(IL100080_REQUEST_DATA)); err != nil {
		t.Errorf("Parsing request message faied, error: %v", err)
	} else {
		t.Logf("Parsing request message success: %++v", il100080I)
	}

	if il100080RequestBody, err := il100080I.PackRequest(); err != nil {
		t.Errorf("Build request message failed, error: %v", err)
	} else {
		t.Logf("Build request message success: %s", il100080RequestBody)
	}

	if err := il100080I.Validate(); err != nil {
		t.Errorf("catch error: %v", err)
	}

	il100080I.MobileNumber = "09540248871"

	if err := il100080I.Validate(); err != nil {
		t.Logf("catch error: %v", err)
	} else {
		t.Errorf("can not catch mobileNumber error")
	}

	invalidRequest := "{ \"Form\": }"

	if err := il100080I.UnPackRequest([]byte(invalidRequest)); err != nil {
		t.Logf("catch error: %v", err)
	} else {
		t.Errorf("can not catch err")
	}

}

func TestIL100080O(t *testing.T) {
	il100080O := &IL100080O{}
	if err := il100080O.UnPackResponse([]byte(IL100080_RESPONSE_DATA)); err != nil {
		t.Errorf("Parsing response message failed, error: %v", err)
	} else {
		t.Logf("Parsing response message success, message: %++v", il100080O)
	}

	if il100080ResponseBody, err := il100080O.PackResponse(); err != nil {
		t.Errorf("catch error: %v", err)
	} else {
		t.Logf("il100080 response body: %s", il100080ResponseBody)
	}
}

