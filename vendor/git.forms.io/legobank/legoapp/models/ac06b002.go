package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC06b002I struct {
	AcctgAcctNo string `valid:"Required;MaxSize(20)"` //贷款核算账号
	DealDate    string `valid:"Required"`
}

type AC06b002O struct {
	BalanceType   string
	PeriodNum     int
	CalcBgnDate   string
	CalcEndData   string
	LastCalcDate  string
	AccmWdAmt     float64
	AcruUnstlIntr float64
	Status        string
}

type AC06b002IDataForm struct {
	FormHead CommonFormHead
	FormData AC06b002I
}

type AC06b002ODataForm struct {
	FormHead CommonFormHead
	FormData AC06b002O
}

type AC06b002RequestForm struct {
	Form []AC06b002IDataForm
}

type AC06b002ResponseForm struct {
	Form []AC06b002ODataForm
}

// @Desc Build request message
func (o *AC06b002RequestForm) PackRequest(AC06b002I AC06b002I) (responseBody []byte, err error) {

	requestForm := AC06b002RequestForm{
		Form: []AC06b002IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC06b002I",
				},
				FormData: AC06b002I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC06b002RequestForm) UnPackRequest(request []byte) (AC06b002I, error) {
	AC06b002I := AC06b002I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC06b002I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC06b002I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC06b002ResponseForm) PackResponse(AC06b002O AC06b002O) (responseBody []byte, err error) {
	responseForm := AC06b002ResponseForm{
		Form: []AC06b002ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC06b002O",
				},
				FormData: AC06b002O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC06b002ResponseForm) UnPackResponse(request []byte) (AC06b002O, error) {

	AC06b002O := AC06b002O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC06b002O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC06b002O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC06b002I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
