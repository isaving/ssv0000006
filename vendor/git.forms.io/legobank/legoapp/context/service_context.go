package context

//Pass the universal chain and common business information through the passing context
type ServiceContext struct {
	TraceId      string
	SpanId       string
	ParentSpanId string
	Topic        string

	LastActEntryNo int //entry no for accounting
	TxDeptCode     string
}
