package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

type ErrResponseForm struct {
	Form []ErrorForm `json:"Form"`
}

type ErrorForm struct {
	FormHead CommonFormHead `json:"FormHead"`
	FormData ErrorFormData  `json:"FormData"`
}

type CommonFormHead struct {
	FormId string `json:"FormId"` //RETMSG
}

type ErrorFormData struct {
	RetMsgCode string
	RetMessage string
}

type ProtoMessageInterface interface {
	UnPackRequest()
}

type CommRequestForm struct {
	Form []CommonForm
}

type CommonForm struct {
	FormHead CommonFormHead
	FormData interface{}
}

// @Desc Build request message
func (o *CommRequestForm) PackRequest(formId string, formData interface{}) (responseBody []byte, err error) {

	responseForm := CommRequestForm{
		Form: []CommonForm{
			{
				FormHead: CommonFormHead{
					FormId: formId,
				},
				FormData: formData,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}
